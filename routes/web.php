<?php
 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserUjianController;


use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\SoalController;
use App\Http\Controllers\SoalGambarController;
use App\Http\Controllers\UjianController;
use App\Http\Controllers\UjianUserController;
use App\Http\Controllers\TypeaheadController;
use App\Http\Controllers\HasilUjianController;

use App\Http\Controllers\JsonController;

use App\Http\Controllers\ExportImportController;
use App\Http\Controllers\ExportImportSoalController;
use App\Http\Controllers\GrafikNilaiUjianController;
 
Route::get('/', function () {
    return view('auth.login');
});
 
Auth::routes();
 
Route::middleware(['auth'])->group(function () {
 
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
 
    Route::middleware(['admin'])->group(function () {
        Route::get('admin', [AdminController::class, 'index']);

        Route::get('/user-admin', [AdminUserController::class, 'index']);
        Route::get('/user-admin/create', [AdminUserController::class, 'create']);
        Route::post('/user-admin/store', [AdminUserController::class, 'store']);
        Route::get('/user-admin/edit/{id}', [AdminUserController::class, 'edit']);
        Route::post('/user-admin/update/{id}', [AdminUserController::class, 'update']);
        Route::delete('/user-admin/delete/{id}', [AdminUserController::class, 'destroy']);

        Route::get('/soal', [SoalController::class, 'index']);
        Route::get('/soal/nogambar', [SoalController::class, 'nogambar']);
        Route::get('/soal/create', [SoalController::class, 'create']);
        Route::post('/soal/store', [SoalController::class, 'store']);
        Route::get('/soal/edit/{id}', [SoalController::class, 'edit']);
        Route::post('/soal/update/{id}', [SoalController::class, 'update']);
        Route::delete('/soal/delete/{id}', [SoalController::class, 'destroy']);


        Route::get('/soal/gambar', [SoalGambarController::class, 'index']);
        Route::get('/soal/create-gambar', [SoalGambarController::class, 'create']);
        Route::post('/soal/store-gambar', [SoalGambarController::class, 'store']);
        Route::get('/soal/edit-gambar/{id}', [SoalGambarController::class, 'edit']);
        Route::post('/soal/update-gambar/{id}', [SoalGambarController::class, 'update']);
        Route::delete('/soal/delete-gambar/{id}', [SoalGambarController::class, 'destroy']);
        Route::get('/soal/json-file-download', [SoalGambarController::class, 'jsonFileDownload']);

        Route::get('/soal/import-gambar', [SoalGambarController::class, 'importView']);
        Route::post('import-soal-gambar', [SoalGambarController::class, 'jsonFileUpload'])->name('import-soal-gambar');

        Route::get('import-soal-view', [ExportImportSoalController::class, 'importExportView']);
        Route::get('export-soal', [ExportImportSoalController::class, 'export'])->name('export-soal');
        Route::post('import-soal', [ExportImportSoalController::class, 'import'])->name('import-soal');
    
        Route::get('/ujian', [UjianController::class, 'index']);
        Route::get('/ujian/create', [UjianController::class, 'create']);
        Route::post('/ujian/store', [UjianController::class, 'store']);
        Route::get('/ujian/edit/{id}', [UjianController::class, 'edit']);
        Route::post('/ujian/update/{id}', [UjianController::class, 'update']);
        Route::delete('/ujian/delete/{id}', [UjianController::class, 'destroy']);
        
        Route::get('/ujian-user/show/{id}', [UjianUserController::class, 'show']);
        Route::get('/ujian-user/create/{id}', [UjianUserController::class, 'create']);
        Route::post('/ujian-user/store/{id}', [UjianUserController::class, 'store']);
        Route::delete('/ujian-user/delete/{id}', [UjianUserController::class, 'destroy']);
        Route::get('export-token/{id}', [UjianUserController::class, 'exportToken'])->name('export-token-user');
        Route::get('export-hasil-ujian/{id}', [UjianUserController::class, 'exportHasilUjian'])->name('exportHasilUjian');
        
        Route::get('/autocomplete-search', [TypeaheadController::class, 'autocompleteSearch']);

        
        Route::get('/hasil-ujian', [HasilUjianController::class, 'index']);
        Route::get('/hasil-ujian/peserta/{id}', [HasilUjianController::class, 'show']);

        Route::get('import-user-view', [ExportImportController::class, 'importExportView']);
        Route::get('export-user', [ExportImportController::class, 'export'])->name('export');
        Route::post('import-user', [ExportImportController::class, 'import'])->name('import-user');

        
        Route::get('/grafik-nilai-ujian', [GrafikNilaiUjianController::class, 'index']);

    });
 
    Route::middleware(['user'])->group(function () {
        Route::get('user', [UserController::class, 'index']);

        Route::get('riwayat-ujian', [UserUjianController::class, 'riwayat']);
        
        Route::get('/ujian/info/{id}', [UserUjianController::class, 'info']);
        Route::get('/ujian/token/{id}', [UserUjianController::class, 'token']);
        Route::post('/ujian/token_check/{id}', [UserUjianController::class, 'token_check']);

        Route::get('/ujian/mulai/{id}', [UserUjianController::class, 'mulai']);
        Route::post('/ujian/submit/{id}', [UserUjianController::class, 'submit']);

        Route::get('/ujian/selesai/{id}', [UserUjianController::class, 'selesai']);

        Route::get('/ujian/mulai-gambar/{id}', [UserUjianController::class, 'mulaiGambar']);
        Route::post('/ujian/submit-gambar/{id}', [UserUjianController::class, 'submitGambar']);
    });
 
    Route::get('logout', function() {
        Auth::logout();
        redirect('/');
    });
 
});