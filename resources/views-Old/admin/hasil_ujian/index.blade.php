@extends('../../layouts.app')

@section('content')

@if(session('success'))
<p class="alert alert-success">{{ session('success') }}</p>
@endif
<div class="card card-default">
    <div class="card-header">
        Hasil Ujian
    </div>
    <div class="card-header">
        <form class="form-inline">
            <div class="form-group mr-1">
                <input class="form-control" type="text" name="q" value="{{ $q}}" placeholder="Pencarian..." />
            </div>
            <div class="form-group mr-1">
                <button class="btn btn-success">Refresh</button>
            </div>
            <div class="form-group mr-1">
                
            </div>
        </form>
    </div>
    <div class="card-body p-0 table-responsive">
        <table class="table table-bordered table-striped table-hover mb-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tgl </th>
                    <th>Token </th>
                    <th>Jumlah Soal</th>
                    <th>Waktu Pengerjaan</th>
                    <th>Status</th>
                    <th>Peserta</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($rows as $row)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->tgl_ujian }}</td>
                <td>{{ $row->token }}</td>
                <td>{{ $row->jumlah_soal }}</td>
                <td>{{ $row->waktu_pengerjaan }} Menit</td>
                <td>
                    
                        @if( $row->status == 1)
                           Aktif
                        @else
                           Tidak Aktif
                        @endif 
                        
                    
                </td>
                <td>
                    <a class="btn btn-sm btn-success" href="{{ url('hasil-ujian/peserta', $row->id ) }}">Data Peserta</a>
                </td>
               
            </tr>
            @endforeach
        </table>
        <br>
        <br>
        <div class="d-flex justify-content-center">
        {{ $rows->links() }}
    </div>
    </div>
</div>
@endsection