@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Selamat datang {{ $user->name }}, berikut adalah daftar Ujian yang dapat anda kerjakan.</div>
 
                <div class="card-body">
                    @if(session('success'))
                    <p class="alert alert-success">{{ session('success') }}</p>
                    @endif
                    <div class="card-body p-0 table-responsive">
                    <table class="table table-bordered table-striped table-hover mb-0">
                        <thead>
                            <tr>
                                <th>Nama Ujian</th>
                                <th>Jumlah Soal</th>
                                <th>Waktu Pengerjaan</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->ujian_name }}</td>
                                <td>{{ $row->jumlah_soal }}</td>
                                <td>{{ $row->waktu_pengerjaan }} menit</td>
                                <td>
                                <a class="btn btn-sm btn-success" href="{{ url('ujian/token', $row->ujian_id) }}">Detail Ujian</a>
                            
                                </td>
                            </tr>
                        @endforeach

                        @if(count($rows) == 0)
                            <tr>
                                <td colspan='5'>Tidak ada Ujian yang dapat diikuti</td>
                            </tr>
                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection