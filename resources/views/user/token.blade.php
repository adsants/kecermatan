@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Form Token</div>

                <div class="card-body">
                @if($errors->any())
                @foreach($errors->all() as $err)
                <p class="alert alert-danger">{{ $err }}</p>
                @endforeach
                @endif

                    <form action="{{ url('ujian/token_check', $row->ujian_id ) }}" method="POST">
            
                        @csrf
                        @method('POST')
                    
                        <div class="form-group row">
                            <div class="col-3">
                            </div>
                            <div class="col-5">
                                <input type="number" required class="form-control input-sm" name="token" placeholder="Masukkan Token">
                     
                            </div>
                            <div class="col-4">
                                 <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection