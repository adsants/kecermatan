<?php

namespace App\Imports;
  
use App\Models\SoalAngkaHilang;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
  
class SoalsImport implements ToModel, WithHeadingRow
{
    protected $jenis_soal;

    function __construct($jenis_soal) {
        $this->jenis_soal = $jenis_soal;
    }
    
    public function model(array $row)
    {

        
        $rowArray = $row['soal_satu'].",".$row['soal_dua'].",".$row['soal_tiga'].",".$row['soal_empat'].",".$row['soal_benar'];

        return new SoalAngkaHilang([
            'data_soal'         => $rowArray,
            'jenis_soal'        => $this->jenis_soal,
            'jawaban_benar'     => $row['soal_benar']
        ]);

    }
}