<?php

namespace App\Http\Controllers;

use App\Models\SoalAngkaHilang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use DB;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title']  = 'Data Bank Soal ';
        $data['q']      = $request->q;
        $data['rows']   = SoalAngkaHilang::where('data_Soal', 'like', '%' . $request->q . '%')->paginate(10);
        return view('admin.soal_angka_hilang..index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['title'] = 'Tambah Bank Soal ';
        
        return view('admin.soal_angka_hilang.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'angka_satu' => 'required',
            'angka_dua' => 'required',
            'angka_tiga' => 'required',
            'angka_empat' => 'required',
            'angka_benar' => 'required',
        ]);

        $soal = $request->angka_satu.",".$request->angka_dua.",".$request->angka_tiga.",".$request->angka_empat.",".$request->angka_benar;
        $input = new SoalAngkaHilang();
        $input->data_soal = $soal;
        $input->jawaban_benar = $request->angka_benar;
        $input->jenis_soal = $request->jenis_soal;

        $input->save();
        return redirect('soal')->with('success', 'Tambah Data Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
   // public function edit(SoalAngkaHilang $soal)
    //{
    //    $data['title'] = 'Ubah Bank Soal ';
    public function edit($id)
    {
        $post = SoalAngkaHilang::findOrFail($id);

        $explodeSoal    =   explode(',',$post->data_soal);
       // dd( $explodeSoal);
        
        $data['row'] = $post;
        $data['soal_satu'] = $explodeSoal[0];
        $data['soal_dua'] = $explodeSoal[1];
        $data['soal_tiga'] = $explodeSoal[2];
        $data['soal_empat'] = $explodeSoal[3];
        $data['soal_benar'] = $explodeSoal[4];
        return view('admin.soal_angka_hilang.edit', $data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $soal = $request->angka_satu.",".$request->angka_dua.",".$request->angka_tiga.",".$request->angka_empat.",".$request->angka_benar;

        DB::table('soal_angka_hilangs')->where('id', $id)->update([
            'data_soal' => $soal,
            'jawaban_benar' => $request->angka_benar,
            'jenis_soal' => $request->jenis_soal,
        ]);
        
        return redirect('soal')->with('success', 'Ubah Data Berhasil');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoalAngkaHilang $id)
    {
        $id->delete();
        return redirect('soal')->with('success', 'Hapus Data Berhasil');
    }
    
}