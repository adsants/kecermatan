<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Auth;
use DB;
 
class UserController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::user();

        $query           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.user_id','=', $data['user']->id)
        ->where('ujians.status','=', 1)
        ->whereNull('ujian_users.nilai')
        ->Paginate(100);
        //dd($post);
        $data['rows']    = $query;

        return view('user.home', $data);
    }
}