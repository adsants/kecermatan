<?php

namespace App\Http\Controllers;

use App\Models\SoalGambar;
use App\Models\JawabanGambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


use Response;
use View;
use File;

use DB;

class SoalGambarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function jsonFileDownload()
    {
        $dataSoals           =  json_encode(DB::table('soal_gambars')->select('id_soal','soal','status')->get());
        
        $fileName = 'soal_gambar.json';
        File::put(public_path('/upload/json/'.$fileName),$dataSoals);
        return Response::download(public_path('/upload/json/'.$fileName));
	}

    public function importView()
    {
       return view('admin/soal_angka_hilang/import_gambar');
    }
     

    public function jsonFileUpload(Request $request)
    {
 
        //dd($request);

        $request->validate([
            'file' => 'required|mimes:json|max:99999999',
        ]);
  
        $fileName = time().'_upload_soal.'.$request->file->extension();  
   
        $request->file->move(public_path('upload/json/'), $fileName);
   
        $json = File::get(public_path('upload/json/'.$fileName));
        $datas = json_decode($json);

        foreach( $datas as  $data){

            $importSoalGambar = new SoalGambar();
            $importSoalGambar->id_soal  = $data->id_soal;
            $importSoalGambar->soal     = $data->soal;
            $importSoalGambar->status    = $data->status;
            
             $importSoalGambar->save();
            

        }
        
        return redirect('soal/gambar')->with('success', 'Import Data Berhasil');
    }


    public function index(Request $request)
    {
        
        $data['title']  = 'Data Bank Soal Bergambar';
        $data['q']      = $request->q;
        $rows           = DB::table('soal_gambars')
        ->select('id_soal')
        ->groupBy('id_soal')
        ->paginate(10);
        

        $html = "";
        foreach($rows as $row){
           $html .= '<div class="row border-bottom-1  mt-2">'.$this->show_soal($row->id_soal).'</div>';
        }

        $data['tableData'] = $html;

        $data['pagianate'] = $rows;
        return view('admin.soal_angka_hilang.gambar_index', $data);
    }

    public function show_soal($id_soal)
    {
        
        $rows  = DB::table('soal_gambars')
        ->select('*')
        ->where('id_soal',$id_soal)
        ->orderBy('id')
        ->get();

        $html = "";

        foreach($rows as $row){
            $html .= '
                
                <div class="col-sm-2">
                    <div class="card p-2">                
                    '.$row->soal.'
                    </div>
                </div>
            
            ';
        }
            $html .= '
            <div class="col-sm-2 text-center ">
                <a class="btn btn-sm btn-warning m-2" href="'.url("soal/edit-gambar", $row->id_soal ).'">Ubah</a>
            
                </div>
            ';

        return $html;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['title'] = 'Tambah Bank Soal ';
        
        return view('admin.soal_angka_hilang.gambar_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jawaban_1' => 'required',
            'jawaban_2' => 'required',
            'jawaban_3' => 'required',
            'jawaban_4' => 'required',
            'jawaban_5' => 'required',
            'status' => 'required',
        ]);
        

        $newIdSoal = time();


        $inputJawaban1 = new SoalGambar();
        $inputJawaban1->id_soal = $newIdSoal;
        $inputJawaban1->soal = $request->jawaban_1;
        if($request->status == '1'){
            $inputJawaban1->status = 'B';
        }
        $inputJawaban1->save();

        $inputJawaban2 = new SoalGambar();
        $inputJawaban2->id_soal = $newIdSoal;
        $inputJawaban2->soal = $request->jawaban_2;
        if($request->status == '2'){
            $inputJawaban2->status = 'B';
        }
        $inputJawaban2->save();

        $inputJawaban3 = new SoalGambar();
        $inputJawaban3->id_soal = $newIdSoal;
        $inputJawaban3->soal = $request->jawaban_3;
        if($request->status == '3'){
            $inputJawaban3->status = 'B';
        }
        $inputJawaban3->save();

        $inputJawaban4 = new SoalGambar();
        $inputJawaban4->id_soal = $newIdSoal;
        $inputJawaban4->soal = $request->jawaban_4;
        if($request->status == '4'){
            $inputJawaban4->status = 'B';
        }
        $inputJawaban4->save();

        $inputJawaban5 = new SoalGambar();
        $inputJawaban5->id_soal = $newIdSoal;
        $inputJawaban5->soal = $request->jawaban_1;
        if($request->status == '5'){
            $inputJawaban5->status = 'B';
        }
        $inputJawaban5->save();



        return redirect('soal/gambar')->with('success', 'Tambah Data Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
   // public function edit(SoalGambar $soal)
    //{
    //    $data['title'] = 'Ubah Bank Soal ';
    public function edit($id)
    {
        $posts = DB::table('soal_gambars')
        ->select('*')
        ->where('id_soal','=',$id)
        ->orderBy('id')
        ->get();
        
      
        $data['rows'] = $posts;
        $data['idSoal'] = $id;
        return view('admin.soal_angka_hilang.gambar_edit', $data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $posts = DB::table('soal_gambars')
        ->select('*')
        ->where('id_soal','=',$id)
        ->orderBy('id')
        ->get();

        $arrayValidate = [];
        foreach($posts as $post){
            $request->validate([
                'soal_'.$post->id => 'required'
            ]);

        }
        $request->validate([
            'status' => 'required'
        ]);
        


        foreach($posts as $post){
            if($request->input('status') ==  $post->id){
                $status =   "B";
            }
            else{
                $status = 'S';
            }

            DB::table('soal_gambars')->where('id', $post->id)->update([
                'soal'     => $request->input('soal_'.$post->id),
                'status'    => $status
            ]);

        }
        
        
        return redirect('soal/gambar')->with('success', 'Ubah Data Berhasil');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoalGambar $id)
    {
        $id->delete();
        return redirect('soal/gambar')->with('success', 'Hapus Data Berhasil');
    }
    
}